use yew::prelude::*;
use wasm_bindgen::prelude::*;
use std::{cell::RefCell, collections::HashMap};
use web_sys::{console, js_sys::Math::{atan2, sqrt}};
use std::time::Duration;
use libp2p::{
    core::{upgrade::Version, Multiaddr}, identify, multiaddr, ping, swarm::{
        NetworkBehaviour, StreamProtocol, SwarmEvent
    }, PeerId, Transport
};
use futures::{pin_mut, FutureExt, StreamExt};
use std::sync::{Arc};
use futures::AsyncWriteExt;
use serde::{Serialize, Deserialize};
use std::str::FromStr;
use yew::events::TouchEvent;


#[derive(Serialize, Deserialize)]
struct JoyStickEvent {
    cardinalDirection: String,
    x: String,
    xPosition: u32,
    y: String,
    yPosition: u32,
}

#[derive(Serialize, Deserialize)]
struct Libp2pJoystickMessage {
    x: i8,
    y: i8,
    direction: String,
}

#[derive(Serialize, Deserialize)]
enum ButtonMode {
    Down,
    Up,
}

#[derive(Serialize, Deserialize)]
struct Libp2pButtonMessage {
    name: String,
    mode: ButtonMode,
}

#[derive(Serialize, Deserialize)]
enum Libp2pMessageType {
    JoystickMessage(Libp2pJoystickMessage),
    ButtonMessage(Libp2pButtonMessage),
}

impl Libp2pMessageType {
    fn new_joystick_msg(x: i8, y: i8, direction: String) -> Self {
        let msg = Libp2pJoystickMessage { x, y, direction };
        Self::JoystickMessage(msg)
    }
    
    fn new_button_msg(name: String, mode: ButtonMode) -> Self {
        let msg = Libp2pButtonMessage { name, mode};
        Self::ButtonMessage(msg)
    }
}

#[wasm_bindgen]
extern "C" {
    type JoyStick;

    #[wasm_bindgen(constructor)]
    fn new(id_name: &str, params: JsValue, f: &Closure<dyn FnMut(JsValue)>) -> JoyStick;
}

#[derive(Properties)]
struct JoystickFuncProp {
    f: Closure<dyn FnMut(JsValue)>,
}


// TODO change to mouse down and mouse up events
#[derive(Properties)]
struct ButtonsFuncProp {
    joy_move_f: Callback<JsValue>,
    a_down_f: &'static Callback<TouchEvent>,
    a_up_f: &'static Callback<TouchEvent>,
    b_down_f: &'static Callback<TouchEvent>,
    b_up_f: &'static Callback<TouchEvent>,
    command_down_f: &'static Callback<TouchEvent>,
    command_up_f: &'static Callback<TouchEvent>,
}

impl PartialEq for JoystickFuncProp {
    fn eq(&self, _other: &Self) -> bool {
        true
    }
}

impl PartialEq for ButtonsFuncProp {
    fn eq(&self, _other: &Self) -> bool {
        true
    }
}

#[derive(Properties)]
struct AppProp {
    joy_move_f: &'static Callback<JsValue>,
    a_down_f: &'static Callback<TouchEvent>,
    a_up_f: &'static Callback<TouchEvent>,
    b_down_f: &'static Callback<TouchEvent>,
    b_up_f: &'static Callback<TouchEvent>,
    command_down_f: &'static Callback<TouchEvent>,
    command_up_f: &'static Callback<TouchEvent>,
}

impl PartialEq for AppProp {
    fn eq(&self, _other: &Self) -> bool {
        true
    }
}

#[function_component]
fn JoyView(props: &ButtonsFuncProp) -> Html {
    // TODO figure out how to also allow clicks on the text

    let a_color = use_state(|| "green");
    let a_down_f = {
        let a_down_f = props.a_down_f.clone();
        let a_color = a_color.clone();
        move |e| {
            a_down_f.emit(e);
            a_color.set("darkgreen");
        }
    };
    let a_up_f = {
        let a_up_f = props.a_up_f.clone();
        let a_color = a_color.clone();
        move |e| {
            a_up_f.emit(e);
            a_color.set("green");
        }
    };

    let b_color = use_state(|| "red");
    let b_down_f = {
        let b_down_f = props.b_down_f.clone();
        let b_color = b_color.clone();
        move |e| {
            b_down_f.emit(e);
            b_color.set("darkred");
        }
    };
    let b_up_f = {
        let b_up_f = props.b_up_f.clone();
        let b_color = b_color.clone();
        move |e| {
            b_up_f.emit(e);
            b_color.set("red");
        }
    };

    let command_button_state = use_state(|| "Up");
    let command_color = use_state(|| "darkgrey");
    let command_down_f = {
        let command_down_f = props.command_down_f.clone();
        let command_color = command_color.clone();
        let command_button_state = command_button_state.clone();
        move |e| {
            command_down_f.emit(e);
            command_color.set("grey");
            command_button_state.set("Down");
        }
    };
    let command_up_f = {
        let command_up_f = props.command_up_f.clone();
        let command_color = command_color.clone();
        let command_button_state = command_button_state.clone();
        move |e| {
            command_up_f.emit(e);
            command_color.set("darkgrey");
            command_button_state.set("Up");
        }
    };


    let html_direction = use_state(|| "C".to_string());
    let joy_touch_move = {
        let joy_move_f = props.joy_move_f.clone();
        let command_button_state = command_button_state.clone();
        let html_direction = html_direction.clone();

        move |e: TouchEvent| {
            e.prevent_default();  // XXX Prevents right click if in an active event listener
            let touches = e.changed_touches();
            for idx in 0..touches.length() {
                let touch = touches.get(idx).unwrap();
                // TODO if target is not circle
                let _target = touch.target().unwrap();
                // TODO how to get the name of the target?
                let x = touch.page_x();
                let y = touch.page_y();
                if x > 200 {continue};
                if y > 200 {continue};
                if x < 0 {continue};
                if y < 0 {continue};

                let x = x - 100;
                let y = y - 100;
                let y = -y;

                // Calculate the cardinal direction
                let direction = {
                    let r = x.pow(2) + y.pow(2);
                    let r = sqrt(r as f64);
                    let angle = atan2(y as f64, x as f64);
                    let angle = angle * 180./3.14159;
                    console::log_1(&format!("r: {:?}; angle: {:?}", r, angle).into());
                    
                    if r < 50. {
                        "C".to_string()
                    } else if angle > 0. {
                        if angle < 45. {
                            "E".to_string()
                        } else if angle < 135. {
                            "N".to_string()
                        } else {
                            "W".to_string()
                        }
                    } else {
                        if angle > -45. {
                            "E".to_string()
                        } else if angle > -135. {
                            "S".to_string()
                        } else {
                            "W".to_string()
                        }
                    }
                };

                if direction != *html_direction {
                    console::log_1(&format!("direction: {:?}; html: {:?}; state: {:?}", direction, *html_direction, *command_button_state).into());
                    html_direction.set(direction.clone());

                    if direction != "C".to_string() && *command_button_state != "Up" {
                        // XXX iphones dont implement the vibrate function, but wasm bindgen will
                        // still call it event though its null/undefined
                        // So we see if its "truthy" and only then return a navigator
                        let navigator = if let Some(window) = web_sys::window() {
                            let navigator = window.navigator();
                            let value = js_sys::Reflect::get(&navigator, &"vibrate".into());
                            match value {
                                Ok(value) => {
                                    if value.is_truthy() {
                                        Some(navigator)
                                    } else {
                                        None
                                    }
                                },
                                _ => None,
                            }
                        } else {
                            None
                        };
                        if let Some(navigator) = navigator {let _ = navigator.vibrate_with_duration(10);};
                    }
                }

                let msg = JoyStickEvent {
                    x: x.to_string(),
                    y: y.to_string(),
                    xPosition: x as u32,
                    yPosition: y as u32,
                    cardinalDirection: direction,
                };
                let msg = serde_wasm_bindgen::to_value(&msg).unwrap();
                joy_move_f.emit(msg);
            }
        }
    };

    let joy_touch_end = {
        let joy_move_f = props.joy_move_f.clone();
        move |e: TouchEvent| {
            e.prevent_default();  // XXX Prevents right click
            let msg = JoyStickEvent {
                x: "0".to_string(),
                y: "0".to_string(),
                xPosition: 0 as u32,
                yPosition: 0 as u32,
                cardinalDirection: "C".to_string()
            };
            let msg = serde_wasm_bindgen::to_value(&msg).unwrap();
            joy_move_f.emit(msg);
        }
    };

    let prevent_zoom = {
        move |e: TouchEvent| {
            e.prevent_default();
        }
    };

    console::log_1(&"Render!!!".into());
    html! {
        <>
            <table>
              <tr>
                <td>
                    <svg height="300" width="650" xmlns="http://www.w3.org/2000/svg">
                        <circle r="100" cx="100" cy="100" fill="grey" ontouchstart={prevent_zoom} ontouchmove={joy_touch_move} ontouchend={joy_touch_end} />
                        <circle r="100" cx="325" cy="100" fill={*a_color} ontouchstart={a_down_f} ontouchend={a_up_f} />
                        <circle r="100" cx="550" cy="100" fill={*b_color} ontouchstart={b_down_f} ontouchend={b_up_f} />
                        <rect width="100" height="50" x="275" y="225" fill={*command_color} ontouchstart={command_down_f} ontouchend={command_up_f} />
                    </svg>
                </td>
              </tr>
            </table>
        </>
    }
}

#[function_component]
fn JoyLogic(props: &JoystickFuncProp) -> Html {
    let param: HashMap<String, String> = HashMap::new();
    let param = serde_wasm_bindgen::to_value(&param).unwrap();
    let _joy = JoyStick::new("joyDiv", param, &props.f);

    html! { <></> }
}


#[function_component]
fn App(props: &AppProp) -> Html {
    let joy_state = use_state(|| Libp2pJoystickMessage {x: 0, y: 0, direction: "C".to_string()});
    let on_change = {
        let joy_state = joy_state.clone();
        let joy_f = props.joy_move_f.clone();
        move |e: JsValue| {
            joy_f.emit(e.clone());
            let event: JoyStickEvent = serde_wasm_bindgen::from_value(e).unwrap();
            let x = i8::from_str(&event.x).unwrap();
            let y = i8::from_str(&event.y).unwrap();
            let msg = Libp2pJoystickMessage {x, y, direction: event.cardinalDirection};
            joy_state.set(msg);
        }
    };
    let on_change = Callback::from(on_change);

    let adv_feat_state = use_state(|| false);
    let adv_feat_button = {
        let adv_feat_state = adv_feat_state.clone();
        move |e: MouseEvent| {
            e.prevent_default();
            adv_feat_state.set(true);
        }
    };

    html! {
        <>
            <JoyView
                joy_move_f={on_change}
                a_down_f={props.a_down_f}
                a_up_f={props.a_up_f}
                b_down_f={props.b_down_f}
                b_up_f={props.b_up_f}
                command_down_f={props.command_down_f}
                command_up_f={props.command_up_f}
            />
            if *adv_feat_state == false {<button onclick={adv_feat_button}>{"Click me to activate advanced features!"}</button>}
            <div>{"x: "} {(*joy_state).x}</div>
            <div>{"y: "} {(*joy_state).y}</div>
            <div>{"direction: "} {(*joy_state).direction.clone()}</div>
        </>
    }
}

fn libp2p_render(addr: &str) {
    #[derive(NetworkBehaviour)]
    struct Behaviour {
        ping: ping::Behaviour,
        identify: identify::Behaviour,
        controller: libp2p_stream::Behaviour,
    }

    let mut swarm = libp2p::SwarmBuilder::with_new_identity()
        .with_wasm_bindgen()
        .with_other_transport(|key| {
            libp2p_webrtc_websys::Transport::new(libp2p_webrtc_websys::Config::new(&key))
        })
        .unwrap()
        .with_other_transport(|key| {
            libp2p::websocket_websys::Transport::default()
                .upgrade(Version::V1)
                .authenticate(libp2p::noise::Config::new(&key).unwrap())
                .multiplex(libp2p::yamux::Config::default())
                .boxed()
        })
        .unwrap()
        .with_behaviour(|keypair| {
            Behaviour {
                ping: ping::Behaviour::new(
                          ping::Config::new()
                          .with_interval(std::time::Duration::new(3, 0))
                          ),
                identify: identify::Behaviour::new(identify::Config::new(
                    "/controller/id/1.0.0".to_string(),
                    keypair.public(),
                )),
                controller: libp2p_stream::Behaviour::new(),
        }})
        .unwrap()
        .with_swarm_config(|c| c.with_idle_connection_timeout(Duration::from_secs(60)))
        .build();

    let mut control = swarm.behaviour().controller.new_control();

    // TODO get this from a URL parameter so we can have links that join games
    console::log_2(&"Dialing addr: ".into(), &addr.into());
    let maddr = addr.parse::<Multiaddr>().unwrap();
    swarm.dial(maddr.clone()).unwrap();
    console::log_1(&"Parsing peer id..".into());
    let peer_id = if let Some(multiaddr::Protocol::P2p(ma)) = maddr.iter().last() {
        if let Ok(peer) = PeerId::try_from(ma) {
            peer
        } else {
            panic!("Cannot parse peerId");
        }
    } else {
        panic!("Cannot parse peerId from multiaddress");
    };

    
    console::log_1(&"Starting thread".into());

    wasm_bindgen_futures::spawn_local(async move {
        console::log_1(&format!("Connecting to: {:?}", peer_id).into());
        // TODO move projects into one and share the stream protocol string
        let fut = control.open_stream(peer_id, StreamProtocol::new("/controller/0.1.0")).fuse();
        pin_mut!(fut);

        console::log_1(&"Got stream future".into());
        let mut r : RefCell<libp2p::Stream>;
        let mut r2 : Arc<RefCell<libp2p::Stream>>;
        loop {
            futures::select! {
                stream = fut => {
                    let stream = stream.unwrap();
                    r = RefCell::new(stream);
                    r2 = Arc::new(r);
                    let joyf_arc = r2.clone();

                    console::log_1(&"Got stream from future".into());
                    let joy_f = move |e| {
                        console::log_2(&"Joystick: ".into(), &e);
                        let event: JoyStickEvent = serde_wasm_bindgen::from_value(e).unwrap();
                        let x = i8::from_str(&event.x).unwrap();
                        let y = i8::from_str(&event.y).unwrap();
                        let msg = Libp2pMessageType::new_joystick_msg( x, y, event.cardinalDirection);
                        let msg = serde_json::to_string(&msg).unwrap();
                        let buf = msg.as_bytes();
                        let mut stream = joyf_arc.borrow_mut();
                        let f = stream.write_all(&buf);
                        console::log_1(&"Starting to send data...".into());
                        let _a = futures::executor::block_on(f).unwrap();
                        console::log_1(&"Sent data!".into());
                    };
                    let joy_f = Callback::from(joy_f);
                    let joy_f = Box::new(joy_f);
                    let joy_f : &'static _ = Box::leak(joy_f);

                    let a_down_arc = r2.clone();
                    let a_down_f = move |e: TouchEvent| {
                        console::log_1(&"Button A pressed".into());
                        e.prevent_default();  // XXX Prevents right click
                        let msg = Libp2pMessageType::new_button_msg( "A".to_string(), ButtonMode::Down);
                        let msg = serde_json::to_string(&msg).unwrap();
                        let buf = msg.as_bytes();
                        let mut stream = a_down_arc.borrow_mut();
                        let f = stream.write_all(&buf);
                        console::log_1(&"Starting to send data...".into());
                        let _a = futures::executor::block_on(f).unwrap();
                        console::log_1(&"Sent data!".into());
                    };
                    let a_down_f = Callback::from(a_down_f);
                    let a_down_f = Box::new(a_down_f);
                    let a_down_f: &'static _ = Box::leak(a_down_f);
                    
                    let b_down_arc = r2.clone();
                    let b_down_f = move |e: TouchEvent| {
                        console::log_1(&"Button B pressed".into());
                        e.prevent_default();  // XXX Prevents right click
                        let msg = Libp2pMessageType::new_button_msg( "B".to_string(), ButtonMode::Down);
                        let msg = serde_json::to_string(&msg).unwrap();
                        let buf = msg.as_bytes();
                        let mut stream = b_down_arc.borrow_mut();
                        let f = stream.write_all(&buf);
                        console::log_1(&"Starting to send data...".into());
                        let _a = futures::executor::block_on(f).unwrap();
                        console::log_1(&"Sent data!".into());
                    };
                    let b_down_f = Callback::from(b_down_f);
                    let b_down_f = Box::new(b_down_f);
                    let b_down_f: &'static _ = Box::leak(b_down_f);

                    let a_up_arc = r2.clone();
                    let a_up_f = move |_e| {
                        console::log_1(&"Button A released".into());
                        let msg = Libp2pMessageType::new_button_msg( "A".to_string(), ButtonMode::Up);
                        let msg = serde_json::to_string(&msg).unwrap();
                        let buf = msg.as_bytes();
                        let mut stream = a_up_arc.borrow_mut();
                        let f = stream.write_all(&buf);
                        console::log_1(&"Starting to send data...".into());
                        let _a = futures::executor::block_on(f).unwrap();
                    };
                    let a_up_f = Callback::from(a_up_f);
                    let a_up_f = Box::new(a_up_f);
                    let a_up_f: &'static _ = Box::leak(a_up_f);


                    let b_up_arc = r2.clone();
                    let b_up_f = move |_e| {
                        console::log_1(&"Button B released".into());
                        let msg = Libp2pMessageType::new_button_msg( "B".to_string(), ButtonMode::Up);
                        let msg = serde_json::to_string(&msg).unwrap();
                        let buf = msg.as_bytes();
                        let mut stream = b_up_arc.borrow_mut();
                        let f = stream.write_all(&buf);
                        console::log_1(&"Starting to send data...".into());
                        let _a = futures::executor::block_on(f).unwrap();
                    };
                    let b_up_f = Callback::from(b_up_f);
                    let b_up_f = Box::new(b_up_f);
                    let b_up_f: &'static _ = Box::leak(b_up_f);

                    let command_down_arc = r2.clone();
                    let command_down_f = move |e: TouchEvent| {
                        console::log_1(&"Button COMMAND pressed".into());
                        e.prevent_default();  // XXX Prevents right click
                        let msg = Libp2pMessageType::new_button_msg( "COMMAND".to_string(), ButtonMode::Down);
                        let msg = serde_json::to_string(&msg).unwrap();
                        let buf = msg.as_bytes();
                        let mut stream = command_down_arc.borrow_mut();
                        let f = stream.write_all(&buf);
                        console::log_1(&"Starting to send data...".into());
                        let _a = futures::executor::block_on(f).unwrap();
                        console::log_1(&"Sent data!".into());
                    };
                    let command_down_f = Callback::from(command_down_f);
                    let command_down_f = Box::new(command_down_f);
                    let command_down_f: &'static _ = Box::leak(command_down_f);
 
                    let command_up_arc = r2.clone();
                    let command_up_f = move |_e| {
                        console::log_1(&"Button COMMAND released".into());
                        let msg = Libp2pMessageType::new_button_msg( "COMMAND".to_string(), ButtonMode::Up);
                        let msg = serde_json::to_string(&msg).unwrap();
                        let buf = msg.as_bytes();
                        let mut stream = command_up_arc.borrow_mut();
                        let f = stream.write_all(&buf);
                        console::log_1(&"Starting to send data...".into());
                        let _a = futures::executor::block_on(f).unwrap();
                    };
                    let command_up_f = Callback::from(command_up_f);
                    let command_up_f = Box::new(command_up_f);
                    let command_up_f: &'static _ = Box::leak(command_up_f);
 
                    let lets_try_this = AppProp {
                        joy_move_f: joy_f,
                        a_down_f: &a_down_f,
                        a_up_f: &a_up_f,
                        b_down_f: &b_down_f,
                        b_up_f: &b_up_f,
                        command_down_f: &command_down_f,
                        command_up_f: &command_up_f,
                    };

                    console::log_1(&"Starting render...".into());
                    yew::Renderer::<App>::with_props(lets_try_this).render();
                    console::log_1(&"Done rendering".into());
                }
                swarm_event = swarm.next() => {
                    let swarm_event = swarm_event.unwrap();
                    match swarm_event {
                        SwarmEvent::Behaviour(BehaviourEvent::Ping( ping::Event { result: Err(e), .. })) => {
                            console::log_1(&format!("Ping failed: {:?}", e).into());

                            break;
                        }
                        SwarmEvent::Behaviour(BehaviourEvent::Ping( ping::Event {
                            peer,
                            result: Ok(rtt),
                            ..
                        })) => {
                            console::log_1(&format!("Ping successful: RTT: {:?}, from {:?}", rtt, peer).into());
                        }
                        SwarmEvent::ConnectionClosed {
                            cause: Some(cause), ..
                        } => {
                            console::log_1(&format!("Swarm event: {:?}", cause).into());

                            if let libp2p::swarm::ConnectionError::KeepAliveTimeout = cause {

                                break;
                            }
                        }
                        evt => console::log_1(&format!("Swarm event: {:?}", evt).into()),
                    }
                },
            }
        }
    });
}

fn test_render() {
    console::log_1(&"TEST RENDER, NOT DIALING ANYONE".into());

    let joy_f = move |e| {
        console::log_2(&"Joystick: ".into(), &e);
    };
    let joy_f = Callback::from(joy_f);
    let joy_f = Box::new(joy_f);
    let joy_f : &'static _ = Box::leak(joy_f);

    let a_down_f = move |e: TouchEvent| {
        console::log_1(&"Button A pressed".into());
        e.prevent_default();  // XXX Prevents right click
    };
    let a_down_f = Callback::from(a_down_f);
    let a_down_f = Box::new(a_down_f);
    let a_down_f: &'static _ = Box::leak(a_down_f);
    
    let b_down_f = move |e: TouchEvent| {
        console::log_1(&"Button B pressed".into());
        e.prevent_default();  // XXX Prevents right click
    };
    let b_down_f = Callback::from(b_down_f);
    let b_down_f = Box::new(b_down_f);
    let b_down_f: &'static _ = Box::leak(b_down_f);

    let a_up_f = move |_e| {
        console::log_1(&"Button A released".into());
    };
    let a_up_f = Callback::from(a_up_f);
    let a_up_f = Box::new(a_up_f);
    let a_up_f: &'static _ = Box::leak(a_up_f);


    let b_up_f = move |_e| {
        console::log_1(&"Button B released".into());
    };
    let b_up_f = Callback::from(b_up_f);
    let b_up_f = Box::new(b_up_f);
    let b_up_f: &'static _ = Box::leak(b_up_f);

    let command_down_f = move |e: TouchEvent| {
        console::log_1(&"Button COMMAND pressed".into());
        e.prevent_default();  // XXX Prevents right click
    };
    let command_down_f = Callback::from(command_down_f);
    let command_down_f = Box::new(command_down_f);
    let command_down_f: &'static _ = Box::leak(command_down_f);
    
    let command_up_f = move |_e| {
        console::log_1(&"Button COMMAND released".into());
    };
    let command_up_f = Callback::from(command_up_f);
    let command_up_f = Box::new(command_up_f);
    let command_up_f: &'static _ = Box::leak(command_up_f);
    
    let lets_try_this = AppProp {
        joy_move_f: joy_f,
        a_down_f: &a_down_f,
        a_up_f: &a_up_f,
        b_down_f: &b_down_f,
        b_up_f: &b_up_f,
        command_down_f,
        command_up_f,
    };

    console::log_1(&"Starting render...".into());
    yew::Renderer::<App>::with_props(lets_try_this).render();
    console::log_1(&"Done rendering".into());
}

fn main() {
    let window = web_sys::window().unwrap();
    let href = window.location().href().unwrap();
    console::log_1(&format!("href: {:?}", href).into()); 
    let parsed_url = url::Url::parse(&href).unwrap();
    let query_part = parsed_url.query();
    if let Some(query) = query_part {
        let queries = query.split('&');
        // TODO support multiple addresses
        let mut addr: String = String::new();
        for query in queries {
            if query.starts_with("addr=") {
                addr = query[5..].to_string();
                console::log_1(&format!("Found addr in query: {:?}", &addr).into()); 
                break;
            }
        }
        libp2p_render(&addr);
    } else {
        test_render();
    };


}
